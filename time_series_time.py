import pandas as pd
from datetime import datetime

def time_series(path_to_df, date_column,
                      transaction_column,
                      # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                      analysis_column_name,
                      product_column='StockCode', user_column='CustomerID',
                      quantity_column='Product Quantity',
                      output_file=None,
                      list_of_selected_items=None,
                      frequency='1D', # '7D' = weeks, '1M' = months, '1H' = hours, '1S' = seconds
                      rolling_window_size=None,
                      start_date=None, #by default start_date is the start date of a df
                      end_date=None, #by default end_date is the end date of a df
                      date_format = '%d/%m/%Y'):

    df_original = pd.read_csv(path_to_df)
    df_original[date_column] = pd.to_datetime(df_original[date_column]).dt.date
    df_original[date_column] = pd.to_datetime(df_original[date_column])

    if list_of_selected_items is not None:
        df = df_original.loc[df_original[analysis_column_name].isin(list_of_selected_items)].copy()
    else:
        df = df_original.copy()
        
    if transaction_column == 'index':
        transaction_column = 'transaction_id'
        df[transaction_column] = df.index

    if analysis_column_name == product_column:
        df_gr = df.groupby([pd.Grouper(key=date_column, freq=frequency), analysis_column_name])[
            quantity_column].sum().unstack().fillna(0)

    elif analysis_column_name == user_column:
        df_grouped_temp = df.groupby(
            [pd.Grouper(key=date_column, freq=frequency),date_column, analysis_column_name, transaction_column], as_index=False).count()
        # counting the number of visits for each user for a considered day of the week
        df_gr =  df_grouped_temp.groupby(
                [pd.Grouper(key=date_column, freq=frequency), analysis_column_name])[transaction_column].count().unstack().fillna(0)
    else:
        return print('analysis_column_name value is not correct')

    if start_date is not None:
        min_date = datetime.strptime(start_date, date_format)
    else:
        min_date = df_original[date_column].min()

    if end_date is not None:
        max_date = datetime.strptime(end_date, date_format)
    else:
        max_date = df_original[date_column].max()

    df_selected_by_time = df_gr.loc[(df_gr.index >= min_date) & (df_gr.index <= max_date)]
    # creation of time series sequence
    time_series = pd.date_range(start=min_date, end=max_date, freq=frequency)
    df_date = pd.DataFrame(index=time_series)
    
    df_merged = df_date.join(df_selected_by_time).fillna(0)

    if rolling_window_size is not None:
        df_rolling = df_merged.rolling(window=rolling_window_size).mean()
    else:
        df_rolling = df_merged.copy()

    df_rolling.loc['Total'] = df_rolling.sum(axis=0)
    df_return = df_rolling.sort_values(by = df_rolling.last_valid_index(), ascending=False, axis=1)

    if output_file is not None:
        df_return.to_csv('{}.csv'.format(output_file))
    return df_return



df_time_series = time_series('kauia_dataset_excluded_extras.csv',
            'Transaction Date',
            'Transaction ID',
            'Member ID',
             frequency = '1D',
             product_column='Product Name',
            user_column='Member ID',
            quantity_column='Product Quantity',
            #list_of_selected_items = ['Yoghurt Pot', 'Steak Cajun Salad'],
            output_file='time_series_users',
            #start_date= '9/9/2019',
            #end_date= '25/10/2019',
            rolling_window_size = 7
            )
