import pandas as pd
import numpy as np


def date_to_unit_of_time(data_frame, date_column_name, normalized_date_column_name,
                    unit_of_time='seconds'):  # unit_of_time = 'seconds' or 'days'

    data_frame[date_column_name] = pd.to_datetime(data_frame[date_column_name])
    minimum_date = data_frame[date_column_name].min()
    if unit_of_time == 'seconds':
        data_frame[normalized_date_column_name] = data_frame[date_column_name].apply(
            lambda date: (date - minimum_date).seconds + (24 * 60 * 60) * (date - minimum_date).days)
    elif unit_of_time == 'days':
        data_frame[normalized_date_column_name] = data_frame[date_column_name].apply(
            lambda date: (date - minimum_date).days)
    elif unit_of_time == 'weeks':
        data_frame[normalized_date_column_name] = data_frame[date_column_name].apply(
            lambda date: (date - minimum_date).days // 7)
    else:
        return print('Not supported value of time units')
    return data_frame

def get_list_top_elements(path_to_dataframe_in_csv, transaction_column, # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                      column_for_analysis_name,
                      product_column='StockCode',
                      user_column='CustomerID',
                      quantity_column='Product Quantity',
                      top_percent_or_number = 'number',
                      n_top_elements = 20):
    df = pd.read_csv(path_to_dataframe_in_csv)

    if column_for_analysis_name == product_column:

        df_grouped = df.groupby([column_for_analysis_name], as_index=False)[
            quantity_column].sum().sort_values(quantity_column, ascending=False)

    elif column_for_analysis_name == user_column:
        df_grouped_temp = df.groupby(
            [column_for_analysis_name, transaction_column], as_index=False).count()

        # counting the number of visits for each user for a considered day of the week
        df_grouped = \
            df_grouped_temp.groupby(
                [column_for_analysis_name],  as_index=False)[transaction_column].count().sort_values(transaction_column, ascending=False)

    else:
        return print('column_for_analysis_name value is not correct')

    if top_percent_or_number == 'number':
        number_top_elements = n_top_elements
    elif top_percent_or_number == 'percent':
        number_top_elements = round(len(df_grouped)*n_top_elements/100)
    else:
        return print('the value of top_percent_or_number is not correct')

    df_top_elements = df_grouped[:number_top_elements]
    return df_top_elements[column_for_analysis_name].unique()

# if column_for_analysis_name == users_column the method counts visits; if column_for_analysis_name == product_column it counts quantity
def time_series(path_to_dataframe_in_csv, date_column,
                      transaction_column,
                      # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                      column_for_analysis_name,
                      unit_of_time = 'days',
                      product_column='StockCode', user_column='CustomerID',
                      quantity_column='Product Quantity',
                      is_normalized=True,
                      output_file=None,
                      list_of_selected_items = None,
                      top_percent_or_number='number',
                      n_top_elements=None,
                      rolling_window_size = None):


    if n_top_elements is not None:
        if list_of_selected_items is None:
            list_of_selected_items = []

        list_top_elements = get_list_top_elements(path_to_dataframe_in_csv,
                                                    transaction_column, # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                                                    column_for_analysis_name,
                                                    product_column,
                                                    user_column,
                                                    quantity_column,
                                                    top_percent_or_number,
                                                    n_top_elements)

        list_of_considered_items = np.append(list_top_elements, list_of_selected_items)
        df_temp = pd.read_csv(path_to_dataframe_in_csv)
        df = df_temp.loc[df_temp[column_for_analysis_name].isin(list_of_considered_items)].copy()
    elif list_of_selected_items is not None:
        df_temp = pd.read_csv(path_to_dataframe_in_csv)
        df = df_temp.loc[df_temp[column_for_analysis_name].isin(list_of_selected_items)].copy()
    else:
        df = pd.read_csv(path_to_dataframe_in_csv)

    # if there is no a transaction column in the data_frame then we create it and assign an index value to the transaction column

    if transaction_column == 'index':
        transaction_column = 'transaction_id'
        df[transaction_column] = df.index

    df_normalized_date = date_to_unit_of_time(df, date_column, 'Normalized Date',   #put additional column: 'Normalized Date' to a df
                                 unit_of_time = unit_of_time)
    date_list = df_normalized_date['Normalized Date'].unique()

    if column_for_analysis_name == product_column:

        df_grouped = df_normalized_date.groupby(['Normalized Date', column_for_analysis_name])[
            quantity_column].sum().unstack().fillna(0)
    elif column_for_analysis_name == user_column:
        df_grouped_temp = df_normalized_date.groupby(
            [column_for_analysis_name, transaction_column, 'Normalized Date'], as_index=False).count()

        # counting the number of visits for each user for a considered day of the week
        df_grouped = \
            df_grouped_temp.groupby(
                ['Normalized Date', column_for_analysis_name])[transaction_column].count().unstack().fillna(0)
    else:
        return print('column_for_analysis_name value is not correct')

    #create a list of columns' name (0th column is a column_for_analysis, so we must exclude it from the list)
    df_grouped.loc['Total'] = df_grouped.sum(axis=0)

    if rolling_window_size is not None:
        string_total = df_grouped.loc[df_grouped.index == 'Total']
        df_rolling = df_grouped.loc[df_grouped.index != 'Total'].rolling(window=rolling_window_size,
                                                                                 min_periods=1).mean()
        df_grouped = pd.concat([df_rolling, string_total])
        print(df_grouped)


    # normalization: create a list of days of the week (columns of a df_grouped_by_column_for_analysis_time_interval) and
    # iterate over them, dividing on a total amount of visits/quantities
    if is_normalized:

        for date in date_list:
            df_grouped.loc[date] = df_grouped.loc[date]/ df_grouped.loc['Total']

    if output_file is not None:
        df_grouped.sort_values(by = df_grouped.last_valid_index(), ascending=False, axis=1).reset_index().to_csv('{}.csv'.format(output_file), index=False)
    return df_grouped.sort_values(by = df_grouped.last_valid_index(), ascending=False, axis=1).reset_index()

df_time_series = time_series('kauia_dataset_excluded_extras.csv',
            'Transaction Date',
            'Transaction ID',
            'Product Name',
             unit_of_time = 'weeks',
             product_column='Product Name',
            user_column='Member ID',
            quantity_column='Product Quantity',
            is_normalized=True,
            list_of_selected_items = None,
            top_percent_or_number='number',
            n_top_elements=20,
            output_file='Check',
            rolling_window_size = 2)

print(df_time_series)